#include <SimpleTimer.h>
// Sim808 Modülü İçin Gerekli Olan Kütüphaneler
#include <DFRobot_sim808.h>
#include <SoftwareSerial.h>
#include <string.h>
#define MESSAGE_LENGTH 160
char message[MESSAGE_LENGTH];
int messageIndex = 0;
char* sendMsg;
String messageConvert;
String phoneConvert;
char phone[16];
char * orderPhone;
char * arr[1];
char * pch;
int i = 0;
int called = 0;
bool callStatus = false;
char datetime[24];
String gsmATResponse;
int buf = 0;
#define PIN_TX 10
#define PIN_RX 11
SoftwareSerial gsmSerial(PIN_TX,PIN_RX);
DFRobot_SIM808 sim808(&gsmSerial);
void sendSMS();
void readSMS();
int allNumbers = 0;
// Loadcell için gerekli olan ayarlamalar ve kütüphanelerin yüklenmesi
#include <hx711.h>
Hx711 scale(A1, A0);
float offset=0;
int weight=0;
int setWeight=20;
void setup() 
{  
  Serial.begin(9600);
  gsmSerial.begin(9600);
  loadcellReset();
  //Sim Kontrolü Yapılıyor.
  if(!sim808.init())
  {
      Serial.print("Hata! Sim Okunamadı\r\n");
      pinMode(4,OUTPUT);
      digitalWrite(4,LOW);
      Serial.println("GSM Modülü Açılıyor...");
      delay(3000);
      digitalWrite(4,HIGH);
      pinMode(4,INPUT);
      delay(4000);
  }
  // Sim kontrolünün gerçekleştiğine dair bilgi ekrana bilgi gönderiliyor.
  Serial.println("Sim Tanımlandı");
  Serial.println("Mesaj Alımı Açıktır.");
  sim808.sendSMS("+905412106355","Sistem Acildi");
  
}

void loop() {
  delay(100);
  weight=scale.getGram()-offset;
  if(weight<1)
  weight=0;
  Serial.print(weight);
  Serial.println(" gram");
  if(called == 1){
      Serial.println("Arama yapılmış.");
  }else{
      Serial.println("Arama yapılmadı.");
   }
  Serial.print("Ayarlanan Sipariş Numarası :");
  Serial.println(orderPhone);
  Serial.print("Ayarlanan Su Sinir Seviyesi :");
  Serial.println(setWeight);
  
  //Serial.print(gsmSerial.readString());
  
  
  if(orderPhone != NULL){
    //Serial.print(arr[1]);
    
    if(called == 0){
      if(weight < setWeight){
        callStatus = sim808.callUp(orderPhone);
        if(gsmSerial.available()>0){
          gsmATResponse = gsmSerial.readString();
          gsmATResponse.trim();
        }
        Serial.println(gsmATResponse);
        called = 1;
      }
    }
  }
  messageIndex = sim808.isSMSunread();
   if (messageIndex > 0)
   { 
      readSMS();
      phoneConvert = String(phone);
      messageConvert = String(message);
      if(messageConvert == "Herkes"){
          allNumbers = 1;
      }else if(messageConvert == "Yonetici"){
          allNumbers = 0;
      }
      if(phoneConvert == "+905412106355" || allNumbers == 1){
          sendSMS();
      }
      Serial.println("Mesaj Alımı Açık");
   }
}
void readSMS()
{
  Serial.print("Mesaj Numarasi: ");
  Serial.println(messageIndex);
  sim808.readSMS(messageIndex, message, MESSAGE_LENGTH, phone, datetime);
  sim808.deleteSMS(messageIndex);
  Serial.print("Mesajı Gönderen Numara: ");
  Serial.println(phone);  
  Serial.print("Tarih: ");
  Serial.println(datetime);        
  Serial.print("Mesaj İçeriği: ");
  Serial.println(message);
}
void sendSMS()
{
  Serial.println("Mesaj Gönderiliyor...");
  Serial.println(phone);
    pch = strtok(message,":");
    i = 0;
    while (pch != NULL)
    {
      arr[i++] = pch;
      pch = strtok(NULL, ":");
    }
    Serial.println(String(arr[0]));
    Serial.println(String(arr[1]));
  if(messageConvert == "Sifirla"){
    loadcellReset();
    sendMsg = "Sistem Sifirlandi.";
  }else if(String(arr[0]) == "SiparisNumarasi"){
    buf = atoi(arr[1]);
    Serial.println(buf);
    if(buf != 0){
      orderPhone = arr[1];
      char textMsg[200];
      sprintf(textMsg, "Ayarlanan siparis numarasi : %s", orderPhone);
      sendMsg = textMsg;
    }
  }else if(String(arr[0]) == "SinirBelirle"){
    buf = atoi(arr[1]);
    Serial.println(buf);
    if(buf != 0){
      setWeight = atoi(arr[1]);
      char textMsg[200];
      sprintf(textMsg, "Ayarlanan su sinir seviyesi : %d gram", setWeight);
      sendMsg = textMsg;
    }
  }else if(messageConvert == "AramaSifirla"){
      called = 0;
      sendMsg = "Arama sifirlanmasi gerceklestirildi.";
  }else if(messageConvert == "Seviye"){
    char charWeight[200];
    sprintf(charWeight, "%d gram", weight);
    sendMsg = charWeight;
  }else if(messageConvert == "Herkes"){
    sendMsg = "Sistem tum numaralara acik hale getirildi.";
  }else if(messageConvert == "Yonetici"){
    sendMsg = "Sistem tum numaralara kapatildi. Sadece yonetici komutlari dinlenecek.";
  }else{
    sendMsg = "Tanimlanan bir komut degil?";
  }
  sim808.sendSMS(phone,sendMsg);
}

void loadcellReset(){
  //Ağırlık Farkı Alınıyor.
  scale.setOffset(8135050);
  scale.setScale(60.082f);
  offset=scale.getGram();
  Serial.print("Fark Ağırlığı :");
  Serial.println(offset); 
}
